functions
=========

.. toctree::
   :maxdepth: 4

   consumption_chart
   consumption_countries
   download_file
   enrich
   gapminder
   gdp_countries_chart
   list_countries
   plot_emissions
   prediction
   scatter_emission_consumption
